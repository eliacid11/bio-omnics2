from flask import Flask, request, jsonify, render_template
import os
from python_on_whales import docker
from blast import blast


app = Flask(__name__, static_folder="app/", static_url_path="/")



@app.route('/')
def index():
    return render_template('index.html')


@app.route('/api/blast/search', methods=['GET'])
def search():
    accession_number = request.args.get('accession_number')
    if accession_number is None or accession_number.strip() == '':
        return jsonify({'error': 'Accession number not provided'}), 400
    
    download_folder = f"{CWD}/blast/queries"
    downloaded_file = download_file_from_ncbi(accession_number, download_folder)
    
    print(f"Accession number downloaded: {accession_number}")  # Agregar esta línea para imprimir el número de acceso
    
    blast_result = blast(accession_number, downloaded_file)
    
    return jsonify({'result': blast_result}), 200

if __name__ == '__main__':
    app.run(debug=True)