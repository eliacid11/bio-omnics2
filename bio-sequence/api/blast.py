import os
import subprocess
import json

CWD = os.getcwd()

def blast(query_id, fasta_file):
    comando = ["blastp", "-db", "/blast/blastdb/pdbaa"]
    output = docker.run(
        image="ncbi/blast",
        remove=True,
        volumes=[(f"{CWD}/blast/blastdb", "/blast/blastdb"), 
                 (f"{CWD}/blast/blastdb_custom", "/blast/blastdb_custom"),
                 (f"{CWD}/blast/fasta", "/blast/fasta"), 
                 (f"{CWD}/blast/queries", "/blast/queries")],
        command=comando + ["-query", fasta_file])
    return output

def download_file_from_ncbi(accession, folder):
    wget_command = f"sudo wget -O {folder}/{accession}.fasta 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={accession}&rettype=fasta'"
    subprocess.run(wget_command, shell=True)

    downloaded_file = f"{folder}/{accession}.fasta"
    if os.path.exists(downloaded_file):
        os.chmod(downloaded_file, 0o644)
        username = os.getlogin()
        subprocess.run(["sudo", "chown", f"{username}:{username}", downloaded_file])
        return downloaded_file
    else:
        raise FileNotFoundError(f"No se pudo descargar el archivo {accession}.fasta en la carpeta {folder}")

def fasta_to_json(accession, folder):
    downloaded_file = download_file_from_ncbi(accession, folder)
    with open(downloaded_file, 'r') as file:
        file_content = file.read()

    # Parse header to extract data within parentheses
    header = file_content.split('\n', 1)[0]
    header_data = header.split('|')[2].strip()
    header_data = header_data.replace('RecName: Full=', '').replace('Contains:', '').replace(';', ',')
    header_data = header_data.split('(')
    header_data = [data.strip(')') for data in header_data if data.strip(')')]

    data = {
        "ncbi_accession": accession,
        "downloaded_file_content": file_content,
        "header_data": header_data
    }
    return json.dumps(data)

ncbi_accession = "P01349"
download_folder = f"{CWD}/blast/queries"

json_data = fasta_to_json(ncbi_accession, download_folder)
print(json_data)
